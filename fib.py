from model import VirtualMachine

vm = VirtualMachine(1024)
push = vm.push
pop = vm.pop
stack = vm.stack


def fib():
    push(vm.bp)
    vm.bp = vm.sp
    
    if stack[vm.sp + 1] == 0:
        vm.cx = 0
        vm.bp = pop()
        return

    elif stack[vm.sp + 1] == 1 or stack[vm.sp + 1] == 2:
        vm.cx = 1
        vm.bp = pop()
        return
    
    push(stack[vm.sp + 1] - 1) # push argument
    fib()
    vm.sp += 1 # cleanup argument
    vm.si = vm.cx
    vm.ax = vm.cx # store return value in accumulator register
    vm.cx = 0

    push(stack[vm.sp + 1] - 2)
    fib()
    vm.sp += 1
    vm.ax += vm.cx # add the result from second fib call
    vm.cx = vm.ax # move from ax back to cx as return value
    vm.ax = 0
    vm.bp = pop()


def main():
    push(vm.bp)
    vm.bp = vm.sp

    push(5)
    fib()
    vm.sp += 1
    print(vm.cx)
    vm.cx = 0

main()