# py-assembly

Model assembly in python. Debugging provides stack view.

```bash
$ make # build example assembly from C source
$ make dump # build example assembly by dumping object files
$ make clean # remove examples/*.dasm and examples/*.asm files
```