all:
	make -C examples -j

dump:
	make -C examples dump -j

clean:
	make -C examples clean