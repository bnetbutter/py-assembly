from collections import UserList

class Stack(UserList):
    """
    Index from top of the list refers to start of the list
    """

    def __getitem__(self, index):
        return self.data[-1 * index - 1]

    def __setitem__(self, index, value):
        self.data[-1 * index - 1] = value
    
    

class VirtualMachine:

    def __init__(self, stack_size):
        self.stack = Stack([0 for _ in range(stack_size)])        
        self.bp = stack_size # stack base pointer
        self.sp = stack_size # stack pointer
        self.cx = 0 # accumulator register
        self.ax = 0 # multi purpose
        self.si = 0 # source register
        self.di = 0 # dest register
    

    @property
    def top(self):
        return self.stack[self.sp - 1:]
    
    def push(self, value):
        self.sp -= 1
        self.stack[self.sp] = value
    
    def pop(self):
        value = self.stack[self.sp]
        self.sp += 1
        return value
    

    