from model import VirtualMachine

vm = VirtualMachine(1024)
push = vm.push
pop = vm.pop
stack = vm.stack

def add():
    push(vm.bp)
    vm.bp = vm.sp
    vm.cx = stack[vm.sp + 1]
    vm.cx +=  stack[vm.sp + 2]
    vm.bp = pop() 

def main():
    push(vm.bp)
    vm.bp = vm.sp
    push(9)
    push(2)
    add()
    vm.sp += 2
    print(vm.cx)
    vm.cx = 0

main()